package boot.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by Mindit on 05-Jul-17.
 */
@RestController
public class HomeController {
    @RequestMapping("/")
    public String home(){
        return "Hello kitty!";
    }
}
