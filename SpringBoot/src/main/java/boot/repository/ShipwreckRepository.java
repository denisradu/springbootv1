package boot.repository;

import boot.model.Shipwreck;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Mindit on 06-Jul-17.
 */
public interface ShipwreckRepository extends JpaRepository<Shipwreck,Long> {
}
