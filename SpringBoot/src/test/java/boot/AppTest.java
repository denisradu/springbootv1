package boot;

import boot.controller.HomeController;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Mindit on 06-Jul-17.
 */
public class AppTest {

    @Test
    public void testApp()
    {
        HomeController hc = new HomeController();
        String result = hc.home();
        assertEquals(result, "Hello kitty!");
    }
}
